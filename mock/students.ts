const students = [
  {
    id: '1',
    name: 'John Smith',
  },
  {
    id: '2',
    name: 'Emma Johnson',
  },
  {
    id: '3',
    name: 'Miguel Aguilera',
  },
  {
    id: '4',
    name: 'Olivia Davis',
  },
  {
    id: '5',
    name: 'William Wilson',
  },
  {
    id: '6',
    name: 'Sophia Lee',
  },
  {
    id: '7',
    name: 'James Taylor',
  },
  {
    id: '8',
    name: 'Emily Martin',
  },
  {
    id: '9',
    name: 'Benjamin Anderson',
  },
  {
    id: '10',
    name: 'Ava Rodriguez',
  },
  {
    id: '11',
    name: 'Liam Martinez',
  },
  {
    id: '12',
    name: 'Mia Garcia',
  },
  {
    id: '13',
    name: 'Taylor Swift',
  },
  // {
  //   id: '14',
  //   name: 'Isabella Perez',
  // },
  // {
  //   id: '15',
  //   name: 'Noah Lopez',
  // },
  // {
  //   id: '16',
  //   name: 'Charlotte Hall',
  // },
  // {
  //   id: '17',
  //   name: 'Jacob Young',
  // },
  // {
  //   id: '18',
  //   name: 'Amelia Turner',
  // },
  // {
  //   id: '19',
  //   name: 'Michaela Adams',
  // },
  // {
  //   id: '20',
  //   name: 'Daniel Walker',
  // },
  // {
  //   id: '21',
  //   name: 'Harper Scott',
  // },
  // {
  //   id: '22',
  //   name: 'Alexander Allen',
  // },
  // {
  //   id: '23',
  //   name: 'Luna Baker',
  // },
  // {
  //   id: '24',
  //   name: 'Matthew Mitchell',
  // },
  // {
  //   id: '25',
  //   name: 'Ella King',
  // },
  // {
  //   id: '26',
  //   name: 'Aiden Green',
  // },
  // {
  //   id: '27',
  //   name: 'Scarlett White',
  // },
  // {
  //   id: '28',
  //   name: 'David Turner',
  // },
  // {
  //   id: '29',
  //   name: 'Abigail Harris',
  // },
  // {
  //   id: '30',
  //   name: 'William Carter',
  // },
  // {
  //   id: '31',
  //   name: 'Sofia Murphy',
  // },
  // {
  //   id: '32',
  //   name: 'James Edwards',
  // },
  // {
  //   id: '33',
  //   name: 'Evelyn Cooper',
  // },
  // {
  //   id: '34',
  //   name: 'Elijah Rivera',
  // },
  // {
  //   id: '35',
  //   name: 'Grace Russell',
  // },
  // {
  //   id: '36',
  //   name: 'Christopher Nelson',
  // },
  // {
  //   id: '37',
  //   name: 'Hannah Lewis',
  // },
  // {
  //   id: '38',
  //   name: 'Samuel Morris',
  // },
  // {
  //   id: '39',
  //   name: 'Lily Clark',
  // },
  // {
  //   id: '40',
  //   name: 'Henry Ward',
  // },
];

export default students;
