const questions = [
  {
    number: '101001',
    answer: '41',
  },
  {
    number: '1101',
    answer: '13',
  },
  {
    number: '11110000',
    answer: '240',
  },
  {
    number: '10010',
    answer: '18',
  },
  {
    number: '1111',
    answer: '15',
  },
  {
    number: '1010',
    answer: '10',
  },
  {
    number: '11001100',
    answer: '204',
  },
  {
    number: '1101101',
    answer: '109',
  },
  {
    number: '1001110',
    answer: '78',
  },
  {
    number: '111000',
    answer: '56',
  },
  {
    number: '10101010',
    answer: '170',
  },
  {
    number: '11010101',
    answer: '213',
  },
  {
    number: '101101',
    answer: '29',
  },
  {
    number: '110110',
    answer: '54',
  },
  {
    number: '10000001',
    answer: '129',
  },
  {
    number: '11111111',
    answer: '255',
  },
  {
    number: '11000011',
    answer: '195',
  },
  {
    number: '10000000',
    answer: '128',
  },
  {
    number: '1110101',
    answer: '109',
  },
  {
    number: '1100110',
    answer: '102',
  },
];
export default questions;
