VERSION --arg-scope-and-set 0.7 

FROM alpine:latest

install-deps:
    FROM node:lts-alpine

    RUN apk add --no-cache libc6-compat
    WORKDIR /app

    # Install dependencies
    COPY --if-exists package.json package-lock.json ./
    RUN npm ci --force
    SAVE ARTIFACT ./node_modules

# Build the app
build-frontend:
    FROM node:lts-alpine
    WORKDIR /app

    COPY +install-deps/node_modules ./node_modules
    
    COPY --if-exists --dir utils components pages public styles mock context package.json package-lock.json next.config.js app.json tsconfig.json ./
    RUN npm run build

    SAVE ARTIFACT ./public
    SAVE ARTIFACT ./.next/standalone /.next/standalone
    SAVE ARTIFACT ./.next/static /.next/static

build-image:
    ARG EARTHLY_GIT_SHORT_HASH
    ARG EARTHLY_GIT_BRANCH

    ARG registry=registry.gitlab.com/binarygame/frontend
    ARG branch
    ARG source=default
    ARG mergeid
    ARG tag

    LET tags = ""

    SET tags = "$EARTHLY_GIT_SHORT_HASH $tags"

    IF [ "$branch" != "" ]
        SET tags = "latest-$branch $tags"
    END
    
    IF [ "$branch" = "production" ]
        SET tags = "latest $tags"
    END

    IF [ "$tag" != "" ]
        SET tags = "$tag $tags"
    END

    IF [ "$source" = "merge_request_event" ]
        SET tags = "mr-$mergeid latest-mr-$mergeid $tags"
    END

    BUILD +build-amd64-image --tags "$tags" --registry "$registry"
    BUILD +build-arm64-image --tags "$tags" --registry "$registry"

# Image for x86_64
build-amd64-image:
    ARG --required tags
    ARG --required registry

    FOR tag IN $tags
        FROM --platform=linux/amd64 node:lts-alpine
        WORKDIR /app

        ENV NODE_ENV production
        
        RUN addgroup --system --gid 1001 nodejs
        RUN adduser --system --uid 1001 nextjs

        COPY +build-frontend/public ./public

        RUN mkdir .next && chown nextjs:nodejs .next

        COPY +build-frontend/.next/standalone ./
        COPY +build-frontend/.next/static ./.next/static
        RUN chown -R nextjs:nodejs *

        USER nextjs
        
        EXPOSE 3000
        ENV PORT 3000
        ENV HOSTNAME "0.0.0.0"

        CMD ["node", "server.js"]
        
        SAVE IMAGE --push "$registry:$tag" "$registry:$tag-amd64"
    END

# Image for arm64
build-arm64-image:
    ARG --required tags
    ARG --required registry

    FOR tag IN $tags
        FROM --platform=linux/arm/v8 node:lts-alpine
        WORKDIR /app

        ENV NODE_ENV production
        
        RUN addgroup --system --gid 1001 nodejs
        RUN adduser --system --uid 1001 nextjs

        COPY +build-frontend/public ./public

        RUN mkdir .next && chown nextjs:nodejs .next

        COPY +build-frontend/.next/standalone ./
        COPY +build-frontend/.next/static ./.next/static
        RUN chown -R nextjs:nodejs *

        USER nextjs
        
        EXPOSE 3000
        ENV PORT 3000
        ENV HOSTNAME "0.0.0.0"

        CMD ["node", "server.js"]
        
        SAVE IMAGE --push "$registry:$tag" "$registry:$tag-arm64"
    END
