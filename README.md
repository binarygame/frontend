## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Building image builds locally

This project makes use of [Earthly](https://earthly.dev/) to build container images for deployment and local testing.

Build project with images as output:
```bash
# This command outputs the image names for your testing
earthly +build-image
```

## CI/CD Pipelines

On this project, [Earthly](https://earthly.dev/) is used to build and publish the images on Gitlab's built-in Container Registry.

The following tags are available from this system:

´latest´ tags:
- `latest`: latest production build _(alias: `latest-production`)_
- `latest-staging`: latest staging build
- `latest-development`: latest development build (git version)
- `latest-mr-<Merge Request ID>`: latest build from a specific merge request.
    - _The merge request id is the internal variant._

Commit-specific tags:
- `<Short Commit Hash>-production`: Available for all commits in the production branch.
- `<Short Commit Hash>-staging`: Available for all commits in the staging branch.
- `<Short Commit Hash>-development`: Available for all commits in the development branch.
