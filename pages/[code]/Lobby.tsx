import { Grid, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import {
  RoomUserEvent,
  SubscribeEventsResponse,
} from '@buf/binarygame_gateway.bufbuild_es/gateway/events/v1/events_pb.js';
import { JoinRoomResponse } from '@buf/binarygame_gateway.bufbuild_es/gateway/rooms/v1/rooms_pb';
import Wrapper from '../../components/Wrapper';
import { useApi } from '../../utils/endpoint';
import { useGameData } from '../../context/GameDataContext';
import handleGameStart from '../../utils/handleGameStart';
import { useAuth } from '../../context/AuthContext';
import WarningPopup from '../../components/ErrorPopup';
import HostLobby from './HostLobby';

function Lobby({
  roomId = '',
  roomData,
}: {
  roomId: string;
  roomData: JoinRoomResponse;
}) {
  console.log(roomData); // vai ser útil pra pegar o room status
  const router = useRouter();
  const { subscribeToRoomEvents, apiJoinRoom } = useApi();
  const [countdown, setCountdown] = useState<number | null>(null);
  const [isRedirecting, setIsRedirecting] = useState(false);
  const [isHost, setIsHost] = useState(false);
  const [isRedirectingHost, setIsRedirectingHost] = useState(false);
  const { authData } = useAuth();
  const { setQuestions } = useGameData();

  useEffect(() => {
    const handleEvent = (event: SubscribeEventsResponse) => {
      switch (event?.event?.eventType) {
        case 9: // EVENT_TYPE_ROOM_USER_LOST_CONNECTION
          if (
            (event?.event?.event.value as RoomUserEvent).userId ===
            authData.userid
          ) {
            setIsRedirecting(true);
          }
          break;
        case 8: // EVENT_TYPE_ROOM_USER_HOST_CHANGED
          if (
            (event?.event?.event.value as RoomUserEvent).userId ===
            authData.userid
          ) {
            setIsHost(true);
            setIsRedirectingHost(true);
          }
          break;
        case 1:
          handleGameStart(setCountdown, router, roomId, setQuestions, event);
          break;
        default:
          console.log(event);
      }
    };

    const handleError = (error: unknown) => {
      console.error('Error receiving events:', error);
    };

    if (roomId) {
      subscribeToRoomEvents(roomId, isHost, handleEvent, handleError);
      const keepUserAlive = setInterval(() => {
        if (!window.location.pathname.includes(roomId) || isHost) {
          clearInterval(keepUserAlive);
          return;
        }
        apiJoinRoom(roomId);
      }, 2000);
    }
  }, []);

  const handleRedirectHome = () => {
    router.push('/');
  };

  return isHost ? (
    <HostLobby roomId={roomId}>
      <WarningPopup
        message="We lost connection to host, you have been randomly picked to be the new host!"
        open={Boolean(isRedirectingHost)}
        severity="success"
        onClose={() => {
          setIsRedirectingHost(false);
        }}
      />
    </HostLobby>
  ) : (
    <Wrapper additionalAction={handleRedirectHome} roomId={roomId}>
      {isRedirecting && (
        <WarningPopup
          message="You have been disconnected from the room"
          open={Boolean(isRedirecting)}
          onClose={() => {
            router.push('/');
            setIsRedirecting(false);
          }}
        />
      )}
      <Grid
        container
        height="96svh"
        alignItems="center"
        justifyContent="space-between"
        direction="column"
        sx={{ paddingTop: '16px' }}
      >
        <Typography variant="h6" color="text.secondary">
          {roomId}
        </Typography>

        <Grid
          container
          alignItems="center"
          justifyContent="center"
          direction="column"
          gap={4}
        >
          {countdown !== null ? (
            <Typography variant="h4">
              Game starts in: {countdown} seconds
            </Typography>
          ) : (
            <Typography variant="h4">
              Please wait for the host to start the game
            </Typography>
          )}
        </Grid>
        <Typography variant="h5" color="text.secondary" />
      </Grid>
    </Wrapper>
  );
}

export default Lobby;
