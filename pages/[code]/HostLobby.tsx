import { useEffect, useState } from 'react';
import { Button, Grid, Paper, Typography, useMediaQuery } from '@mui/material';
import { useRouter } from 'next/router';
import {
  RoomUserEvent,
  SubscribeEventsResponse,
} from '@buf/binarygame_gateway.bufbuild_es/gateway/events/v1/events_pb.js';
import Wrapper from '../../components/Wrapper';
import { useApi } from '../../utils/endpoint';
import WarningPopup from '../../components/ErrorPopup';
import { useAuth } from '../../context/AuthContext';
import handleGameStart from '../../utils/handleGameStart';
import { useGameData } from '../../context/GameDataContext';

type StudentType = {
  userId: string;
  emoji: string;
  nickname: string;
};

export default function HostLobby({
  roomId = '',
  children,
}: {
  roomId: string;
  children?: React.ReactNode;
}) {
  const router = useRouter();
  const isMobile = useMediaQuery('(max-width: 600px)');
  const [students, setEstudents] = useState<StudentType[]>([]);
  const [isStarting, setIsStarting] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [startGameError, setStartGameError] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState(false);
  const [countdown, setCountdown] = useState<number | null>(null);
  const [isRedirecting, setIsRedirecting] = useState(false);
  const { headers, authData } = useAuth();
  const { setQuestions } = useGameData();
  const { subscribeToRoomEvents, apiStartMatch, apiJoinRoom } = useApi();

  const handleRedirectHome = () => {
    router.push('/');
  };

  useEffect(() => {
    const handleEvent = (event: SubscribeEventsResponse) => {
      switch (event?.event?.eventType) {
        case 9: // EVENT_TYPE_ROOM_USER_LOST_CONNECTION
          if (
            (event?.event?.event.value as RoomUserEvent).userId ===
            authData.userid
          ) {
            setIsRedirecting(true);
          }
          setEstudents((prevStudents) =>
            prevStudents.filter(
              (student) =>
                student.userId !==
                (event?.event?.event.value as RoomUserEvent).userId
            )
          );
          break;
        case 8: // EVENT_TYPE_ROOM_USER_HOST_CHANGED
          setIsRedirecting(true);
          break;
        case 4: // EVENT_TYPE_ROOM_USER_JOINED
          setEstudents((prevStudents) => [
            ...prevStudents,
            event?.event?.event.value as unknown as StudentType,
          ]);
          break;
        case 1: // EVENT_TYPE_ROOM_STARTING
          handleGameStart(setCountdown, router, roomId, setQuestions, event);
          break;
        default:
          console.log(event);
          break;
      }
    };

    const handleError = (error: unknown) => {
      console.error('Error receiving events:', error);
    };

    if (roomId) {
      subscribeToRoomEvents(roomId, isRedirecting, handleEvent, handleError);
      const keepHostAlive = setInterval(() => {
        if (
          !(
            window.location.pathname.includes(roomId) ||
            window.location.pathname.includes('host')
          ) ||
          isRedirecting
        ) {
          clearInterval(keepHostAlive);
          return;
        }
        apiJoinRoom(roomId);
      }, 2000);
    }
  }, []);

  const buttonEnable = () => {
    if (students.length > 0 && !isStarting) {
      setDisabled(false);
      return;
    }
    setDisabled(true);
  };

  useEffect(() => {
    buttonEnable();
  }, [students, isStarting]);

  const displayedStudents = isMobile ? students.slice(0, 5) : students;

  const handleStartGame = () => {
    setIsStarting(true);
    setIsLoading(true);

    if (headers) {
      apiStartMatch(roomId)
        .then(() => {
          setIsStarting(false);
        })
        .catch((error) => {
          console.error(error);
          setStartGameError(true);
          setIsStarting(false);
          setIsLoading(false);
        });
    }
  };

  return (
    <Wrapper additionalAction={handleRedirectHome} roomId={roomId}>
      {children}
      {isRedirecting && (
        <WarningPopup
          message="You have been disconnected from the room"
          open={Boolean(isRedirecting)}
          onClose={() => {
            setIsRedirecting(false);
            router.push('/');
          }}
        />
      )}
      {startGameError && (
        <WarningPopup
          message="Something went wrong to start the game, please try again"
          open={Boolean(startGameError)}
          onClose={() => setStartGameError(false)}
        />
      )}
      <Grid
        container
        height="96vh"
        alignItems="center"
        justifyContent="space-between"
        direction="column"
        sx={{ paddingTop: '24px' }}
      >
        <Grid container alignItems="center" direction="column" gap={1}>
          <Typography
            variant="h2"
            sx={{ display: 'flex', paddingX: 2, textAlign: 'center' }}
          >
            {roomId}
          </Typography>
          <Typography
            color="text.secondary"
            sx={{ display: 'flex', textAlign: 'center' }}
          >
            Show room code to the players
          </Typography>
        </Grid>
        <Grid container spacing={2} justifyContent="center" alignItems="center">
          {displayedStudents.map((student) => (
            <Grid item key={student.userId}>
              <Paper
                sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: '10px',
                  transition: 'background-color 0.3s',
                  minWidth: isMobile ? '50px' : '80px',
                  minHeight: isMobile ? '50px' : '80px',
                  padding: '8px',
                }}
                elevation={4}
              >
                <Grid
                  container
                  direction="column"
                  justifyContent="center"
                  alignItems="center"
                  spacing={1}
                >
                  <Grid item>
                    <Typography variant="h5">{student.emoji}</Typography>
                  </Grid>
                  <Grid item>
                    <Typography>{student.nickname}</Typography>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          ))}
        </Grid>
        <Grid container alignItems="center" direction="column">
          {countdown !== null ? (
            <Typography variant="h4">
              Game starts in: {countdown} seconds
            </Typography>
          ) : (
            <Button
              size="large"
              disabled={disabled || isLoading}
              variant="contained"
              onClick={handleStartGame}
            >
              {isLoading ? 'Starting...' : 'Start Game'}
            </Button>
          )}
        </Grid>
      </Grid>
    </Wrapper>
  );
}
