import React, { useState } from 'react';
import {
  Grid,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import { yellow } from '@mui/material/colors';

interface IconGridProps {
  setOpen: (open: boolean) => void;
  onSelectEmote: (emote: string) => void;
  emotes: string[];
  initiallySelectedEmote?: string;
}

export default function IconGrid({
  setOpen,
  onSelectEmote,
  emotes = [''],
  initiallySelectedEmote,
}: IconGridProps) {
  const [selectedEmote, setSelectedEmote] = useState(initiallySelectedEmote);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const handleEmoteClick = (emote: string) => {
    onSelectEmote(emote);
    setSelectedEmote(emote);
    setOpen(false);
  };

  return (
    <Grid
      container
      spacing={2}
      justifyContent="center"
      alignItems="center"
      sx={{ padding: 2 }}
    >
      {emotes.map((emote, index) => (
        <Grid item key={index}>
          <Paper
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              cursor: 'pointer',
              borderRadius: '10px',
              transition: 'background-color 0.3s',
              backgroundColor:
                selectedEmote === emote ? yellow[300] : '#282828',
              '&:hover': {
                backgroundColor: yellow[300],
              },
              minWidth: isMobile ? '50px' : '80px',
              minHeight: isMobile ? '50px' : '80px',
              padding: '8px',
            }}
            elevation={4}
            onClick={() => handleEmoteClick(emote)}
          >
            <Typography variant={isMobile ? 'h5' : 'h4'}>{emote}</Typography>{' '}
          </Paper>
        </Grid>
      ))}
    </Grid>
  );
}
