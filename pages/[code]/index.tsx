import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Lobby from './Lobby';
import RegistrationScreen from './RegistrationScreen';
import useJoinRoom from '../../utils/data-hooks/useJoinRoom';
import WarningPopup from '../../components/ErrorPopup';
import { useAuth } from '../../context/AuthContext';

export default function GameInit() {
  const [isRegistered, setIsRegistered] = useState(false);
  const [selectedEmote, setSelectedEmote] = useState<string>('');
  const [isJoining, setIsJoining] = useState(false);
  const router = useRouter();
  const [error, setError] = useState(false);
  const [roomCode, setRoomCode] = useState<string>('');
  const { headers, isAuthenticated } = useAuth();

  const { joinRoomData, error: joinError } = useJoinRoom(
    roomCode as string,
    isJoining,
    setIsJoining
  );

  useEffect(() => {
    if (router.isReady) {
      const { code } = router.query;
      setRoomCode(code as string);
      if (isAuthenticated) {
        setIsRegistered(true);
      }
    }
  }, [router.isReady]);

  useEffect(() => {
    if (isRegistered && headers) {
      setIsJoining(true);
    }
  }, [isRegistered, headers]);

  useEffect(() => {
    if (joinError) {
      setError(true);
    }
  }, [joinError]);

  return isRegistered && joinRoomData ? (
    <Lobby roomId={roomCode} roomData={joinRoomData} />
  ) : (
    <>
      {error && (
        <WarningPopup
          message="The room doesn't exist"
          open={Boolean(error)}
          onClose={() => {
            setError(false);
            router.push('/');
          }}
        />
      )}
      <RegistrationScreen
        setIsRegistered={setIsRegistered}
        selectedEmote={selectedEmote}
        setSelectedEmote={setSelectedEmote}
      />
    </>
  );
}
