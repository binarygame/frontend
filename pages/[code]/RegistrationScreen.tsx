import {
  Alert,
  Avatar,
  Badge,
  Button,
  CircularProgress,
  Fade,
  Grid,
  Modal,
  TextField,
  Typography,
  useTheme,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import { useRouter } from 'next/router';
import Wrapper from '../../components/Wrapper';
import IconGrid from './IconGrid';
import useEmotesData from '../../utils/data-hooks/useEmotes';
import getRandomElement from '../../utils/getRandomElement';
import { apiRegisterUser } from '../../utils/endpoint';
import { useAuth } from '../../context/AuthContext';

export default function RegistrationScreen({
  setIsRegistered,
  selectedEmote,
  setSelectedEmote,
  isHost = false,
}: {
  setIsRegistered: React.Dispatch<React.SetStateAction<boolean>>;
  selectedEmote: string;
  setSelectedEmote: React.Dispatch<React.SetStateAction<string>>;
  isHost?: boolean;
}) {
  const theme = useTheme();
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const [username, setUsername] = useState('');
  const { emotes, error: emotesError } = useEmotesData();
  const [isRegistering, setIsRegistering] = useState(false);
  const { setAuthData, isAuthenticated } = useAuth();
  const disabled: boolean = username.length < 3;

  const handleRegister = async () => {
    if (!isRegistering) {
      try {
        setIsRegistering(true);
        const registerData = await apiRegisterUser(username, selectedEmote);
        const userid = registerData.data?.id || '';
        const privatekey = registerData.data?.privateKey || '';
        setAuthData(userid, privatekey, selectedEmote, username);
        setIsRegistered(true);
      } catch (err) {
        setIsRegistering(false);
        console.error(err);
      }
    }
  };

  useEffect(() => {
    if (emotes.length > 0) {
      setSelectedEmote(getRandomElement(emotes));
    }
  }, [emotes]);

  useEffect(() => {
    if (isAuthenticated) {
      setIsRegistered(true);
    }
  }, [isAuthenticated, setIsRegistered]);

  return (
    <>
      {emotesError && <Alert severity="error">{emotesError}</Alert>}
      <Wrapper additionalAction={() => router.push('/')}>
        <Grid
          container
          height="88vh"
          alignItems="center"
          justifyContent="center"
          direction="column"
          gap={3}
          sx={{ paddingTop: '16px' }}
        >
          <Badge
            overlap="circular"
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            badgeContent={
              <div>
                <IconButton
                  style={{ color: 'white', backgroundColor: 'gray' }}
                  onClick={() => setOpen(true)}
                >
                  <EditIcon />
                </IconButton>
                <Modal
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  open={open}
                  onClose={() => setOpen(false)}
                  closeAfterTransition
                >
                  <Fade in={open}>
                    <div>
                      <IconGrid
                        setOpen={setOpen}
                        onSelectEmote={(emote) => setSelectedEmote(emote)}
                        emotes={emotes}
                        initiallySelectedEmote={selectedEmote}
                      />
                    </div>
                  </Fade>
                </Modal>
              </div>
            }
          >
            <Avatar
              sx={{
                width: 128,
                height: 128,
                bgcolor: theme.palette.primary.light,
              }}
            >
              <Typography sx={{ fontSize: 64 }}>{selectedEmote}</Typography>
            </Avatar>
          </Badge>
          <Grid
            container
            alignItems="center"
            justifyContent="center"
            direction="column"
            gap={2}
            sx={{ paddingTop: '16px' }}
          >
            <TextField
              id="username"
              onChange={(e) => setUsername(e.target.value)}
              label="Insert name"
              variant="outlined"
              inputProps={{ maxLength: 24 }}
              sx={{ width: 240 }}
            />
          </Grid>
          {isRegistering ? (
            <CircularProgress size={28} color="inherit" />
          ) : (
            <Button
              size="medium"
              disabled={disabled}
              onClick={handleRegister}
              variant="contained"
            >
              {isHost ? 'Create room' : 'Join'}
            </Button>
          )}
        </Grid>
      </Wrapper>
    </>
  );
}
