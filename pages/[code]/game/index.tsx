import { Box, Grid, TextField, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Question } from '@buf/binarygame_gateway.bufbuild_es/gateway/events/v1/events_pb';
import { Duration } from '@bufbuild/protobuf';
import { SubmitGuessRequest } from '@buf/binarygame_gateway.bufbuild_es/gateway/guesses/v1/guesses_pb';
import Wrapper from '../../../components/Wrapper';
import { useGameData } from '../../../context/GameDataContext';
import parseQueryType from '../../../utils/parseQueryType';
import { useApi } from '../../../utils/endpoint';

const questionInitialState = new Question({
  id: 'abcd',
  query: '0',
  answer: '0',
  type: 1,
});

export default function Game() {
  const [questionData, setQuestionData] = useState<Question>(
    questionInitialState as Question
  );
  const [parsedType, setParsedType] = useState({
    from: 'Binary',
    to: 'Decimal',
  });
  const [score, setScore] = useState(0);
  const [userAnswer, setUserAnswer] = useState('');
  const [error, setError] = useState(false);
  const [roomCode, setRoomCode] = useState<string | undefined>('');
  const [currentIndex, setCurrentIndex] = useState(0);
  const router = useRouter();
  const { questions } = useGameData();
  const { apiSubmitGuess } = useApi();

  const handleUserAnswerChange = (e: { target: { value: string } }) => {
    const { value } = e.target;
    if (/^[0-9]*$/.test(value)) {
      setUserAnswer(value);
    }
  };

  const getQuestion = () => {
    const currentQuestion = questions.questions[currentIndex];
    setQuestionData(currentQuestion);
    setParsedType(parseQueryType(currentQuestion.type));
    setCurrentIndex(currentIndex + 1);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      handleAnswerSubmit();
    }
  };

  const handleAnswerSubmit = () => {
    setUserAnswer('');
    const isCorrect = userAnswer === questionData.answer;
    apiSubmitGuess({
      roomId: (roomCode as string) || '',
      questionId: questionData.id,
      isCorrect,
      timeTaken: new Duration({ nanos: 0 }),
    } as SubmitGuessRequest).then(() => {
      if (isCorrect) {
        setScore(score + 1);
      } else {
        setError(true);
      }
      getQuestion();
    });
  };

  const handleRedirectHome = () => {
    router.push('/');
  };

  useEffect(() => {
    if (userAnswer !== '') {
      setError(false);
    }
  }, [userAnswer]);

  useEffect(() => {
    if (router.isReady && questions?.questions?.length) {
      const { code } = router.query as { code: string };
      setRoomCode(code);

      getQuestion();
    }
  }, [router.isReady, questions]);

  return (
    <Wrapper additionalAction={handleRedirectHome} roomId={roomCode}>
      <Grid
        container
        height="96svh"
        alignItems="center"
        justifyContent="space-between"
        direction="column"
        sx={{ paddingTop: '16px' }}
      >
        <Typography variant="h6" color="text.secondary">
          {roomCode}
        </Typography>

        <Grid
          container
          alignItems="center"
          justifyContent="center"
          direction="column"
          gap={6}
        >
          <div>
            <Typography component="span" color="text.secondary">
              Convert{' '}
            </Typography>
            <Typography component="span" sx={{ fontWeight: 'bold' }}>
              {parsedType.from}
            </Typography>
            <Typography component="span" color="text.secondary">
              {' '}
              to{' '}
            </Typography>
            <Typography component="span" sx={{ fontWeight: 'bold' }}>
              {parsedType.to}
            </Typography>
          </div>
          <Box sx={{ fontSize: 64 }} className={error ? 'shake' : undefined}>
            {questionData.query}
          </Box>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TextField
              id="answer"
              color="primary"
              value={userAnswer}
              onChange={handleUserAnswerChange}
              label="Answer"
              variant="outlined"
              inputProps={{ step: 'any' }}
              onKeyDown={handleKeyDown}
            />
          </Box>
        </Grid>

        <Typography variant="h5" color="text.secondary">
          {score}
        </Typography>
      </Grid>
    </Wrapper>
  );
}
