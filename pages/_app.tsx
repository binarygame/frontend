import React from 'react';
import { CacheProvider, EmotionCache } from '@emotion/react';
import { CssBaseline } from '@mui/material';
import Head from 'next/head';
import createEmotionCache from '../utils/createEmotionCache';
import '../styles/globals.css';
import ThemeProvider from '../components/ThemeProvider';
import { AuthProvider } from '../context/AuthContext';
import { GameDataProvider } from '../context/GameDataContext';

const clientSideEmotionCache = createEmotionCache();

function MyApp(props: {
  Component: React.ComponentClass;
  emotionCache?: EmotionCache;
  pageProps: object;
}) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>Beenario</title>
        <link rel="icon" href="/icons/binIcon.png" />
      </Head>
      <AuthProvider>
        <ThemeProvider>
          <CssBaseline />
          <GameDataProvider>
            <Component {...pageProps} />
          </GameDataProvider>
        </ThemeProvider>
      </AuthProvider>
    </CacheProvider>
  );
}

export default MyApp;
