import { useEffect, useState } from 'react';
import useCreateRoom from '../../utils/data-hooks/useCreateRoom';
import WarningPopup from '../../components/ErrorPopup';
import RegistrationScreen from '../[code]/RegistrationScreen';
import HostLobby from '../[code]/HostLobby';
import { useAuth } from '../../context/AuthContext';

export default function GameInit() {
  const [isRegistered, setIsRegistered] = useState(false);
  const [selectedEmote, setSelectedEmote] = useState<string>('');
  const [isCreating, setIsCreating] = useState(false);
  const [error, setError] = useState(false);
  const { headers, isAuthenticated } = useAuth();
  const roomName = 'xpto';

  const { roomId = '', error: creationError } = useCreateRoom(
    isCreating,
    setIsCreating,
    roomName
  );

  useEffect(() => {
    if (isAuthenticated) {
      setIsRegistered(true);
    }
  }, [isAuthenticated]);

  useEffect(() => {
    if (isRegistered && headers) {
      setIsCreating(true);
    }
  }, [isRegistered, headers]);

  useEffect(() => {
    if (creationError) {
      setError(true);
    }
  }, [creationError]);

  return isRegistered && roomId !== '' ? (
    <HostLobby roomId={roomId} />
  ) : (
    <>
      {error && (
        <WarningPopup
          message="Something went wrong creating the room"
          open={Boolean(error)}
          onClose={() => setError(false)}
        />
      )}
      <RegistrationScreen
        setIsRegistered={setIsRegistered}
        selectedEmote={selectedEmote}
        setSelectedEmote={setSelectedEmote}
        isHost
      />
    </>
  );
}
