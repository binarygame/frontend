import SchoolIcon from '@mui/icons-material/School';
import BadgeIcon from '@mui/icons-material/Badge';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Grid, Typography, TextField, Stack, Button } from '@mui/material';
import useIsMobile from '../utils/isMobile';
import { apiCheckIfRoomExists } from '../utils/endpoint';
import WarningPopup from '../components/ErrorPopup';
import Wrapper from '../components/Wrapper';

export default function Home() {
  const router = useRouter();
  const isMobile = useIsMobile();
  const [input, setInput] = useState('');
  const [code, setCode] = useState('');
  const [error, setError] = useState(false);

  useEffect(() => {
    const checkRoom = async () => {
      if (code.length === 4) {
        const res = await apiCheckIfRoomExists(code);
        if (res.exists) {
          router.push(`/${code}`);
        } else {
          setError(true);
        }
      }
    };
    checkRoom();
  }, [code, router]);

  const handleCodeChange = (e: { target: { value: string } }) => {
    const { value } = e.target;
    setInput(value);
    setError(false);
  };

  const disabled = input.length < 4;

  const handleRedirectTeacher = () => {
    router.push('/host');
  };

  function handleKeyDown(event: React.KeyboardEvent<HTMLDivElement>) {
    if (event.key === 'Enter') {
      event.preventDefault();
      setCode(input);
    }
  }

  return (
    <Wrapper additionalAction={() => {}}>
      <WarningPopup
        message="Error joining this room"
        open={Boolean(error)}
        onClose={() => setError(false)}
      />
      <Grid
        container
        height="96vh"
        alignItems="center"
        justifyContent="center"
        direction="column"
        gap={6}
        // sx={{ marginRight: '-64px' }}
      >
        <Typography
          variant={isMobile ? 'h3' : 'h2'}
          sx={{ display: 'flex', paddingX: 2, textAlign: 'center' }}
        >
          Welcome to Beenario
        </Typography>

        <Stack direction="column" rowGap={2}>
          <TextField
            id="code"
            size="medium"
            value={input}
            onChange={handleCodeChange}
            label="Room code"
            variant="outlined"
            color="primary"
            inputProps={{ maxLength: 4 }}
            sx={{ width: 260 }}
            onKeyDown={handleKeyDown}
          />
          <Button
            size="medium"
            onMouseDown={() => setCode(input)}
            color="primary"
            variant="outlined"
            disabled={disabled}
            sx={{ width: 260, alignSelf: 'center' }}
            startIcon={<SchoolIcon />}
          >
            Enter game
          </Button>
          <Button
            size="medium"
            onMouseDown={handleRedirectTeacher}
            variant="outlined"
            sx={{ width: 260, alignSelf: 'center' }}
            color="secondary"
            startIcon={<BadgeIcon />}
          >
            Host game
          </Button>
        </Stack>
      </Grid>
    </Wrapper>
  );
}
