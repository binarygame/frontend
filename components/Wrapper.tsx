import { Box } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import ModeNightIcon from '@mui/icons-material/ModeNight';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import { ReactNode } from 'react';
import LogoutIcon from '@mui/icons-material/Logout';
import { useAuth } from '../context/AuthContext';
import { useThemeContext } from './ThemeProvider';
import { useApi } from '../utils/endpoint';

export default function Wrapper(props: {
  children: ReactNode;
  roomId?: string;
  additionalAction: () => void;
}) {
  const { isAuthenticated, setAuthData } = useAuth();
  const { theme, toggleTheme } = useThemeContext();
  const { apiLeaveRoom } = useApi();

  const handleLogout = () => {
    if (props.roomId) {
      apiLeaveRoom(props.roomId);
    }
    setAuthData('', '', '', '');
    props.additionalAction();
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <Box sx={{ justifyContent: 'end', padding: '16px', width: '64px' }}>
        <span>
          {isAuthenticated && (
            <IconButton color="error" onClick={handleLogout}>
              <LogoutIcon />
            </IconButton>
          )}
        </span>
      </Box>
      {props.children}
      <Box sx={{ justifyContent: 'end', padding: '16px' }}>
        <IconButton onClick={toggleTheme} sx={{ display: 'flex-auto' }}>
          {theme.palette.mode === 'light' ? (
            <ModeNightIcon />
          ) : (
            <WbSunnyIcon color="primary" />
          )}
        </IconButton>
      </Box>
    </Box>
  );
}
