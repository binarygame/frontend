import { useState, useEffect } from 'react';

function RegressiveCount({
  initialValue,
  onTimeEnd,
}: {
  initialValue: number;
  onTimeEnd: () => void;
}) {
  const [count, setCount] = useState(initialValue);

  useEffect(() => {
    const intervalId = setInterval(() => {
      if (count > 0) {
        setCount(count - 1);
      } else {
        clearInterval(intervalId); // Stop the interval when count reaches 0
        if (onTimeEnd) {
          onTimeEnd(); // Call the callback function
        }
      }
    }, 1000);

    return () => {
      clearInterval(intervalId);
    };
  }, [count, onTimeEnd]);

  return (
    <div>
      <h1>Starting game in {count} seconds</h1>
    </div>
  );
}

export default RegressiveCount;
