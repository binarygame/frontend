import { createContext, ReactNode, useContext, useState } from 'react';
import {
  createTheme,
  Palette,
  ThemeProvider as MuiThemeProvider,
} from '@mui/material';

interface MyTheme {
  palette: Palette; // Replace with your actual theme properties
}

const lightTheme = createTheme({
  typography: {
    fontFamily: 'Mitr',
  },
  palette: {
    mode: 'light',
    primary: {
      main: '#282828',
      light: '#454027',
    },
    secondary: {
      main: '#282828',
    },
    background: {
      default: '#EECE64',
    },
    text: {
      primary: '#282828',
    },
  },
});

const darkTheme = createTheme({
  typography: {
    fontFamily: 'Mitr',
  },
  palette: {
    mode: 'dark',
    primary: {
      main: '#EEC43B',
      light: '#FFEC98',
    },
    secondary: {
      main: '#dda15e',
    },
    text: {
      primary: '#EEC43B',
      secondary: '#F5F5F5',
    },
  },
});

const ThemeContext = createContext<{
  theme: MyTheme;
  toggleTheme: () => void;
}>({
  theme: darkTheme, // Set initial theme
  toggleTheme: () => {},
});

export const useThemeContext = () => useContext(ThemeContext);

function ThemeProvider({ children }: Readonly<{ children: ReactNode }>) {
  const [themeMode, setThemeMode] = useState('dark');

  const toggleTheme = () => {
    setThemeMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
  };

  const theme = themeMode === 'light' ? lightTheme : darkTheme;

  return (
    // eslint-disable-next-line react/jsx-no-constructed-context-values
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>
    </ThemeContext.Provider>
  );
}

export default ThemeProvider;
