import React, { useCallback } from 'react';
import { Alert, Snackbar } from '@mui/material';

interface GenericErrorPopupProps {
  severity?: 'error' | 'warning' | 'info' | 'success';
  message: string;
  open: boolean;
  onClose: () => void;
}

export default function WarningPopup({
  severity = 'error',
  message,
  open,
  onClose,
}: GenericErrorPopupProps) {
  const handleClose = useCallback(
    (event?: React.SyntheticEvent | Event, reason?: string) => {
      if (reason === 'clickaway') {
        return;
      }
      onClose();
    },
    [onClose]
  );
  return (
    <Snackbar
      open={open}
      onClose={handleClose}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      autoHideDuration={5000}
    >
      <Alert onClose={onClose} severity={severity} variant="filled">
        {message}
      </Alert>
    </Snackbar>
  );
}
