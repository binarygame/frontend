import React, { createContext, useContext, useState, ReactNode } from 'react';
import { QuestionSet } from '@buf/binarygame_gateway.bufbuild_es/gateway/events/v1/events_pb.js';

type GameDataType = {
  questions: QuestionSet;
  setQuestions: (questions: QuestionSet) => void;
};

const initialGameData: GameDataType = {
  questions: {
    questions: [],
    id: '',
    level: 0,
  } as unknown as QuestionSet,
  setQuestions: () => {},
};

const GameDataContext = createContext<GameDataType>(initialGameData);

function GameDataProvider({ children }: { children: ReactNode }) {
  const [questions, setQuestions] = useState<QuestionSet>(
    initialGameData.questions
  );

  const value = React.useMemo(() => ({ questions, setQuestions }), [questions]);

  return (
    <GameDataContext.Provider value={value}>
      {children}
    </GameDataContext.Provider>
  );
}

const useGameData = () => {
  const context = useContext(GameDataContext);
  return context;
};

export { GameDataProvider, useGameData };
