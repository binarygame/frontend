import React, {
  createContext,
  useContext,
  ReactNode,
  useState,
  useEffect,
} from 'react';
import { setCookie, getCookie } from 'cookies-next';

type AuthDataType = {
  userid: string;
  privateKey: string;
  emote: string;
  username: string;
};

type AuthContextType = {
  headers: {
    Authorization: string;
  };
  setAuthData: (
    userid: string,
    privateKey: string,
    emote: string,
    username: string
  ) => void;
  isAuthenticated: boolean;
  authData: AuthDataType;
};

const initialState = {
  headers: {
    Authorization: '',
  },
  setAuthData: () => {},
  isAuthenticated: false,
  authData: {
    userid: '',
    privateKey: '',
    emote: '',
    username: '',
  },
};

const AuthContext = createContext<AuthContextType>(initialState);

function AuthProvider({ children }: { children: ReactNode }) {
  const [authData, setAuthDataState] = useState<AuthDataType>(
    initialState.authData
  );

  // Load data from cookies on initial render
  useEffect(() => {
    const storedAuthData = getCookie('authData');

    if (storedAuthData) {
      try {
        const parsedAuthData = JSON.parse(storedAuthData as string);
        setAuthDataState(parsedAuthData);
      } catch (error) {
        console.error('Error parsing stored auth data', error);
      }
    }
  }, []);

  const setAuthData = (
    userid: string,
    privateKey: string,
    emote: string,
    username: string
  ) => {
    const newAuthData = { userid, privateKey, emote, username };

    // Update state
    setAuthDataState(newAuthData);

    // Save to cookies
    setCookie('authData', JSON.stringify(newAuthData), {
      req: undefined,
      res: undefined,
      maxAge: 60 * 60, // 1 hour
    });
  };

  const headers = authData && {
    Authorization: `Basic ${btoa(`${authData.userid}:${authData.privateKey}`)}`,
  };

  const isAuthenticated =
    !!authData.userid &&
    !!authData.privateKey &&
    !!authData.emote &&
    !!authData.username;

  const value: AuthContextType = React.useMemo(
    () => ({ headers, setAuthData, isAuthenticated, authData }),
    [headers, setAuthData, isAuthenticated, authData]
  );

  return (
    <AuthContext.Provider value={value as AuthContextType}>
      {children}
    </AuthContext.Provider>
  );
}

const useAuth = () => {
  const context = useContext(AuthContext);
  return context;
};

export { AuthProvider, useAuth };
