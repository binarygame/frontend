function getRandomElement<T>(arr: T[]): T {
  if (!arr.length) {
    return '' as T;
  }
  const randomIndex = Math.floor(Math.random() * arr.length);
  return arr[randomIndex];
}

export default getRandomElement;
