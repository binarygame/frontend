export default function parseQueryType(type: number): {
  from: string;
  to: string;
} {
  switch (type) {
    case 1:
      return {
        from: 'Binary',
        to: 'Decimal',
      };
    case 2:
      return {
        from: 'Decimal',
        to: 'Binary',
      };
    default:
      return {
        from: 'Binary',
        to: 'Decimal',
      };
  }
}
