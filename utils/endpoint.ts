import { UsersService } from '@buf/binarygame_gateway.connectrpc_es/gateway/users/v1/users_connect';
import { RoomsService } from '@buf/binarygame_gateway.connectrpc_es/gateway/rooms/v1/rooms_connect';
import { CallOptions, createPromiseClient } from '@connectrpc/connect';
import { createConnectTransport } from '@connectrpc/connect-web';
import { EventsService } from '@buf/binarygame_gateway.connectrpc_es/gateway/events/v1/events_connect';
import { GuessesService } from '@buf/binarygame_gateway.connectrpc_es/gateway/guesses/v1/guesses_connect';
import { SubscribeEventsResponse } from '@buf/binarygame_gateway.bufbuild_es/gateway/events/v1/events_pb.js';
import { SubmitGuessRequest } from '@buf/binarygame_gateway.bufbuild_es/gateway/guesses/v1/guesses_pb.js';
import { useAuth } from '../context/AuthContext';

const transport = createConnectTransport({
  baseUrl: 'https://bingame-dev.zerowhy.org/',
  useBinaryFormat: false,
});

const usersClient = createPromiseClient(UsersService, transport);
const roomsClient = createPromiseClient(RoomsService, transport);
const eventsClient = createPromiseClient(EventsService, transport);
const guessesClient = createPromiseClient(GuessesService, transport);

export async function apiGetEmotes() {
  return usersClient.getAllowedEmojis({});
}

export async function apiRegisterUser(nickname: string, emoji: string) {
  return usersClient.register({ nickname, emoji });
}

export const apiCheckIfRoomExists = async (roomId: string) => {
  return roomsClient.checkIfRoomExists({ roomId });
};

export const useApi = () => {
  const { headers } = useAuth();
  const options: CallOptions = {
    headers: new Headers(headers),
  };

  const apiJoinRoom = async (roomId: string) =>
    roomsClient.joinRoom({ roomId }, options);

  const apiCreateRoom = async (roomName: string) =>
    roomsClient.createRoom({ name: roomName }, options);

  const apiSubscribeRoom = async (roomId: string) =>
    eventsClient.subscribeEvents({ roomId }, options);

  const subscribeToRoomEvents = async (
    roomId: string,
    stop: boolean,
    onEvent: (event: SubscribeEventsResponse) => void,
    onError: (error: unknown) => void
  ) => {
    try {
      const stream = eventsClient.subscribeEvents({ roomId }, options);

      // eslint-disable-next-line no-restricted-syntax
      for await (const event of stream) {
        if (stop) {
          return;
        }
        onEvent(event as unknown as SubscribeEventsResponse);
      }
    } catch (error) {
      onError(error);
    }
  };

  const apiStartMatch = async (roomId: string) =>
    roomsClient.startMatch({ roomId }, options);

  const apiLeaveRoom = async (roomId: string) =>
    roomsClient.leaveRoom({ roomId }, options);

  const apiSubmitGuess = async (guessData: SubmitGuessRequest) =>
    guessesClient.submitGuess(guessData, options);

  return {
    apiJoinRoom,
    apiCheckIfRoomExists,
    apiCreateRoom,
    apiSubscribeRoom,
    subscribeToRoomEvents,
    apiStartMatch,
    apiLeaveRoom,
    apiSubmitGuess,
  };
};
