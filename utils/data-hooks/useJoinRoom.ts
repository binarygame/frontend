import { useEffect, useState } from 'react';
import { type JoinRoomResponse } from '@buf/binarygame_gateway.bufbuild_es/gateway/rooms/v1/rooms_pb';
import { useApi } from '../endpoint';

function useJoinRoom(
  roomId: string,
  isJoining: boolean,
  setIsJoining: React.Dispatch<React.SetStateAction<boolean>>
) {
  const [joinRoomData, setJoinRoomData] = useState<JoinRoomResponse | null>(
    null
  );
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const { apiJoinRoom } = useApi();

  useEffect(() => {
    if (isJoining && roomId) {
      setIsLoading(true);
      apiJoinRoom(roomId)
        .then((response) => {
          setJoinRoomData(response as unknown as JoinRoomResponse);
        })
        .catch((err) => {
          setError(err);
        })
        .finally(() => {
          setIsLoading(false);
          setIsJoining(false);
        });
    }
  }, [isJoining, roomId]);

  return { joinRoomData, isLoading, error };
}

export default useJoinRoom;
