import { useState, useEffect } from 'react';
import { type RegisterData } from '@buf/binarygame_gateway.bufbuild_es/gateway/users/v1/users_pb';
import { apiRegisterUser } from '../endpoint';

export default function useRegisterUser(
  nickname: string,
  emoji: string,
  isRegistering: boolean,
  setIsRegistering: React.Dispatch<React.SetStateAction<boolean>>
) {
  const [registrationData, setRegistrationData] = useState<RegisterData>();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    if (!nickname || !emoji || isRegistering === false) return;
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const response = await apiRegisterUser(nickname, emoji);
        setRegistrationData(response.data as unknown as RegisterData);
      } catch (err) {
        setError('Failed to register user');
      } finally {
        setIsLoading(false);
        setIsRegistering(false);
      }
    };

    fetchData();
  }, [nickname, emoji, isRegistering]);

  return { registrationData, isLoading, error };
}
