import { useState, useEffect } from 'react';
import { apiGetEmotes } from '../endpoint';

export default function useEmotesData() {
  const [emotes, setEmotes] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const response = await apiGetEmotes();
        setEmotes(response.data);
      } catch (err) {
        setError('Failed to fetch emotes');
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  return { emotes, isLoading, error };
}
