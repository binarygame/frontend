import { useState, useEffect } from 'react';
import { type CreateRoomResponse } from '@buf/binarygame_gateway.bufbuild_es/gateway/rooms/v1/rooms_pb';
import { useApi } from '../endpoint';
import { useAuth } from '../../context/AuthContext';

function useCreateRoom(
  isCreating: boolean,
  setIsCreating: React.Dispatch<React.SetStateAction<boolean>>,
  roomName: string
) {
  const [createRoomData, setCreateRoomData] =
    useState<CreateRoomResponse | null>(null);
  const [isLoading, setIsLoading] = useState(false);
  const { isAuthenticated } = useAuth();
  const [error, setError] = useState<string | null>(null);
  const { apiCreateRoom } = useApi();

  useEffect(() => {
    if (isCreating && roomName && isAuthenticated) {
      setIsLoading(true);

      apiCreateRoom(roomName)
        .then((response) => {
          setCreateRoomData(response as unknown as CreateRoomResponse);
        })
        .catch((err) => {
          console.error(err);
          setError('Failed to create room');
        })
        .finally(() => {
          setIsLoading(false);
          setIsCreating(false);
        });
    }
  }, [isCreating, roomName]);

  return { roomId: createRoomData?.data?.roomId, isLoading, error };
}

export default useCreateRoom;
