import {
  QuestionSet,
  SubscribeEventsResponse,
  RoomStartingEvent,
} from '@buf/binarygame_gateway.bufbuild_es/gateway/events/v1/events_pb';
import { useRouter } from 'next/router';

export default function handleGameStart(
  setCountdown: React.Dispatch<React.SetStateAction<number | null>>,
  router: ReturnType<typeof useRouter>,
  roomId: string,
  setQuestions: (questions: QuestionSet) => void,
  event: SubscribeEventsResponse
) {
  let count = 5;
  setCountdown(count);

  if (event?.event?.event?.value) {
    const data = event?.event?.event?.value as RoomStartingEvent;
    setQuestions(data.questions as QuestionSet);
  }

  const interval = setInterval(() => {
    count -= 1;
    setCountdown(count);
    if (count === 0) {
      clearInterval(interval);
      router.push(`/${roomId}/game`);
    }
  }, 1000);
}
